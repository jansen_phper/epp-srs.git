<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://srs.micang.com All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <jansen.shi@qq.com>
// +----------------------------------------------------------------------
function autoload($class){
    $className = str_replace('\\', '/', str_replace('jansen\\srs\\', '', $class));
    $filePath = __DIR__.'/src/'.$className.'.php';
    if (is_readable($filePath)){
        require $filePath;
    }
}
spl_autoload_register('autoload');