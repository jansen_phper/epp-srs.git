<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://srs.micang.com All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <jansen.shi@qq.com>
// +----------------------------------------------------------------------
namespace jansen\srs;
use Metaregistrar\EPP\eppException;
class SRS{
    /**
     * @var array $registries 支持的注册局列表
     */
    private static $registries = ['verisign', 'pir'];
    /**
     * 构建对应的注册局实例
     * @param string $registry 注册局名称
     * @param array  $config   接口配置
     *                         host     require     核验服务器地址
     *                         port     require     核验服务器端口
     *                         username require     核验服务器登录名
     *                         password require     核验服务器登录密码
     *                         certpath optional    证书路径
     *                         certpwd  optional    证书密码
     *                         self_signed optional 是否允许自签名证书
     *                         logpath  optional    日志文件路径
     *                         timeout  optional    超时时间，默认10秒
     * @return mixed
     * @author:Jansen <jansen.shi@qq.com>
     */
    public static function getInstance(string $registry, array $config){
        $registry = strtolower($registry);
        if (!in_array($registry, self::$registries)){
            throw new eppException('不支持指定的注册局接口。');
        }
        if (empty($config['host']) || empty($config['port']) || empty($config['username']) || empty($config['password'])){
            throw new eppException('缺少对应的注册局连接信息。');
        }
        $logpath = (isset($config['logpath']) && !empty($config['logpath']))?$config['logpath']:null;
        $class = 'jansen\\srs\\drivers\\'.ucfirst($registry);
        $instance = new $class((bool)$logpath);
        //设置服务器信息
        $instance->setHost($config['host'], $config['port']);
        //设置登录信息
        $instance->setAccount($config['username'], $config['password']);
        //设置证书信息
        if (isset($config['certpath']) && isset($config['certpwd'])){
            $instance->enableCertification($config['certpath'], $config['certpwd']);
        }elseif(isset($config['certpath'])){
            $instance->enableCertification($config['certpath'], null);
        }
        if (isset($config['self_signed']) && $config['self_signed']){
            $instance->enableAllowSelfSigned();
        }
        //设置日志文件信息
        if ($logpath){
            $instance->setLogFile($config['logpath']);
        }
        if (isset($config['timeout']) && $config['timeout']>0){
            $instance->setTimeout($config['timeout']);
        }
        return $instance;
    }
}